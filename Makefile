PROJECT_NAME := "protorepo"
PKG := "github.com/tmlamb/$(PROJECT_NAME)"
#PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
#GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all protogen

all: protogen

protogen:
	if ! which protoc > /dev/null; then \
		echo "error: protoc not installed" >&2; \
		exit 1; \
	fi
	go get -u -v github.com/golang/protobuf/protoc-gen-go
	for file in $$(git ls-files '*.proto'); do \
		protoc -I $$(dirname $$file) --go_out=plugins=grpc:$$(dirname $$file) $$file; \
	done
